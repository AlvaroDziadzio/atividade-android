package com.example.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private EditText num;
    private SeekBar seekBar;
    private TextView tipText;
    private TextView totalText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        num = findViewById(R.id.amount);
        seekBar = findViewById(R.id.barTip);
        tipText = findViewById(R.id.textTip);
        totalText = findViewById(R.id.textTotal);

        num.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                recalculate();
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                recalculate();
                ((TextView)findViewById(R.id.textTipAmount)).setText(progress + "%");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        recalculate();

    }

    public void recalculate() {
        float amount = Float.parseFloat(this.num.getText().toString().equals("") ? "0" : this.num.getText().toString());
        int tipAmount = seekBar.getProgress();

        float tip = amount/100 * tipAmount;

        tipText.setText(String.format(Locale.getDefault(), "R$ %.2f", tip));
        totalText.setText(String.format(Locale.getDefault(), "R$ %.2f", tip + amount));

    }

}
